//
//  UserAnnotation.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 06/06/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit
import MapKit

class UserAnnotation: MKPointAnnotation {
    
    var id: String
    
    init(id: String) {
        self.id = id
    }
    
}
