//
//  FishContainerCellDelegate.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 25/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import Foundation


protocol FishContainerCellDelegate: class {
    func transferFishArrayAndSelectedIndexPath(fishArray: [FishModel], indexPath: IndexPath)
}
