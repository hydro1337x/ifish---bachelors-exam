//
//  OfflineModeViewController.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 24/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class OfflineModeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, FishContainerCellDelegate, PondsContainerCellDelegate {
    
    
    
    
    let fishContainerCellId = "fishContainerCellId"
    let pondContainerCellId = "pondContainerCellId"
    
    let bottomPaddingView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.CustomColors.midnightBlue
        return view
    }()
    
    let menuBarContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.CustomColors.midnightBlue
        return view
    }()
    
    let fishButton: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "fish_icon")?.withRenderingMode(.alwaysTemplate)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(image, for: .normal)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        button.tintColor = UIColor.darkGray
        button.addTarget(self, action: #selector(handleFishButtonTapped), for: .touchUpInside)
        return button
    }()
    
    let pondsButton: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "pond_icon")?.withRenderingMode(.alwaysTemplate)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(image, for: .normal)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        button.tintColor = UIColor.darkGray
        button.addTarget(self, action: #selector(handlePondsButtonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.isScrollEnabled = true
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        return cv

    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        collectionView.register(FishContainerCell.self, forCellWithReuseIdentifier: fishContainerCellId)
        collectionView.register(PondsContainerCell.self, forCellWithReuseIdentifier: pondContainerCellId)
        
    }
    
    override func viewDidLayoutSubviews() {
        setupBottomPaddingView()
    }
    
    func setupViews() {
        setupMenuBarContainerView()
        setupFishButton()
        setupPondsButton()
        setupCollectionView()
    }
    
    func setupBottomPaddingView() {
        view.addSubview(bottomPaddingView)
        bottomPaddingView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        bottomPaddingView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomPaddingView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        bottomPaddingView.heightAnchor.constraint(equalToConstant: view.safeAreaInsets.bottom).isActive = true
    }
    
    func setupMenuBarContainerView() {
        view.addSubview(menuBarContainerView)
        menuBarContainerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        menuBarContainerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        menuBarContainerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        menuBarContainerView.heightAnchor.constraint(equalToConstant: 75).isActive = true
    }
    
    func setupFishButton() {
        menuBarContainerView.addSubview(fishButton)
        fishButton.centerYAnchor.constraint(equalTo: menuBarContainerView.centerYAnchor).isActive = true
        fishButton.leftAnchor.constraint(equalTo: menuBarContainerView.leftAnchor, constant: 50).isActive = true
        fishButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        fishButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupPondsButton() {
        menuBarContainerView.addSubview(pondsButton)
        pondsButton.centerYAnchor.constraint(equalTo: menuBarContainerView.centerYAnchor).isActive = true
        pondsButton.rightAnchor.constraint(equalTo: menuBarContainerView.rightAnchor, constant: -50).isActive = true
        pondsButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        pondsButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupCollectionView() {
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -75).isActive = true
        self.fishButton.tintColor = .white
        self.pondsButton.tintColor = .darkGray
    }
    
    @objc func handleFishButtonTapped() {
        scrollToMenuIndex(menuIndex: 0)
        UIView.animate(withDuration: 0.5) {
            self.fishButton.tintColor = .white
            self.pondsButton.tintColor = UIColor.darkGray
        }
    }
    
    @objc func handlePondsButtonTapped() {
        scrollToMenuIndex(menuIndex: 1)
        UIView.animate(withDuration: 0.5) {
            self.fishButton.tintColor = UIColor.darkGray
            self.pondsButton.tintColor = .white
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: fishContainerCellId, for: indexPath) as! FishContainerCell
            cell.delegate = self
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: pondContainerCellId, for: indexPath) as! PondsContainerCell
            cell.delegate = self
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - (view.safeAreaInsets.top + view.safeAreaInsets.bottom + 75))
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        if x == 0 {
            UIView.animate(withDuration: 0.5) {
                self.fishButton.tintColor = .white
                self.pondsButton.tintColor = .darkGray
            }
        }
        else if x == collectionView.frame.width {
            UIView.animate(withDuration: 0.5) {
                self.fishButton.tintColor = .darkGray
                self.pondsButton.tintColor = .white
            }
        }
        
    }
    
    func scrollToMenuIndex(menuIndex: Int) {
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func transferFishArrayAndSelectedIndexPath(fishArray: [FishModel], indexPath: IndexPath) {
        let fishPresenterVC = FishPresenterViewController()
        fishPresenterVC.fishArray = fishArray
        fishPresenterVC.indexPath = indexPath
        navigationController?.show(fishPresenterVC, sender: self)
    }
    
    func transferPondArrayAndSelectedIndexPath(pondArray: [PondModel], indexPath: IndexPath) {
        let pondPresenterVC = PondPresenterViewController()
        pondPresenterVC.pondArray = pondArray
        pondPresenterVC.indexPath = indexPath
        navigationController?.show(pondPresenterVC, sender: self)
    }

}

