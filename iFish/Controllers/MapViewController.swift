//
//  MapViewController.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 02/06/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import SVProgressHUD

class MapViewController: UIViewController, MKMapViewDelegate {
    
    var usersArray: [UserModel]!
    var locationsArray: [LocationModel]!
    var locationManager: CLLocationManager?
    
    let annotationID = "customAnnotationId"
    
    var locationSharingLabelLeftConstraint: NSLayoutConstraint?
    
    let topContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let locationSharingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Location sharing"
        label.textColor = .white
        return label
    }()
    
    let locationSwitch: UISwitch = {
        let sw = UISwitch()
        sw.translatesAutoresizingMaskIntoConstraints = false
        sw.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
        return sw
    }()
    
    let bottomContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let refreshUsersLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Refresh fishermen location"
        label.textColor = .white
        return label
    }()
    
    lazy var refreshButton: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "refresh_icon")
        let tintedImage = image?.withRenderingMode(.alwaysTemplate)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(tintedImage, for: .normal)
        button.tintColor = .white
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(handleRefreshButtonPressed), for: .touchUpInside)
        return button
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.translatesAutoresizingMaskIntoConstraints = false
        ai.alpha = 0
        ai.color = .white
        return ai
    }()
    
    let topSeparatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    let bottomSeparatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    lazy var mapView: MKMapView = {
        let mv = MKMapView()
        mv.translatesAutoresizingMaskIntoConstraints = false
        mv.delegate = self
        return mv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.CustomColors.midnightBlue
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: annotationID)
        setupViews()
        setupLocationManager()
        updateMapViewWithFetchedUsersAndLocations()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let myAnnotation = annotation as? UserAnnotation {
            if myAnnotation.id == FirebaseManager.getUserUID() {
                let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationID, for: myAnnotation) as! MKMarkerAnnotationView
                annotationView.markerTintColor = .red
                return annotationView
            } else {
                let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationID, for: myAnnotation) as! MKMarkerAnnotationView
                annotationView.markerTintColor = .blue
                return annotationView
            }
        }
        print("No annotation returned?")
        return nil
    }
    
    func setupViews() {
        setupLogoutButton()
        setupTopContainerView()
        setupLocationSharingLabel()
        setupActivityIndicator()
        setupLocationSwitch()
        setupTopSeparatorView()
        setupBottomContainerView()
        setupRefreshUsersLabel()
        setupRefreshButton()
        setupBottomSeparatorView()
        setupMapView()
    }
    
    func setupTopContainerView() {
        view.addSubview(topContainerView)
        topContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        topContainerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topContainerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        topContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupLocationSharingLabel() {
        topContainerView.addSubview(locationSharingLabel)
        locationSharingLabel.centerYAnchor.constraint(equalTo: topContainerView.centerYAnchor).isActive = true
        locationSharingLabelLeftConstraint = locationSharingLabel.leftAnchor.constraint(equalTo: topContainerView.leftAnchor, constant: 16)
        locationSharingLabelLeftConstraint?.isActive = true
        locationSharingLabel.widthAnchor.constraint(equalToConstant: 130).isActive = true
        locationSharingLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    func setupLocationSwitch() {
        topContainerView.addSubview(locationSwitch)
        locationSwitch.centerYAnchor.constraint(equalTo: topContainerView.centerYAnchor).isActive = true
        locationSwitch.rightAnchor.constraint(equalTo: topContainerView.rightAnchor, constant: -16).isActive = true
    }
    
    func setupTopSeparatorView() {
        view.addSubview(topSeparatorView)
        topSeparatorView.topAnchor.constraint(equalTo: topContainerView.bottomAnchor).isActive = true
        topSeparatorView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        topSeparatorView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topSeparatorView.heightAnchor.constraint(equalToConstant: (1.0 / UIScreen.main.scale)).isActive = true
    }
    
    func setupBottomContainerView() {
        view.addSubview(bottomContainerView)
        bottomContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        bottomContainerView.topAnchor.constraint(equalTo: topSeparatorView.bottomAnchor).isActive = true
        bottomContainerView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        bottomContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupRefreshUsersLabel() {
        bottomContainerView.addSubview(refreshUsersLabel)
        refreshUsersLabel.centerYAnchor.constraint(equalTo: bottomContainerView.centerYAnchor).isActive = true
        refreshUsersLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        refreshUsersLabel.widthAnchor.constraint(equalToConstant: 250).isActive = true
        refreshUsersLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupRefreshButton() {
        bottomContainerView.addSubview(refreshButton)
        refreshButton.centerYAnchor.constraint(equalTo: bottomContainerView.centerYAnchor).isActive = true
        refreshButton.rightAnchor.constraint(equalTo: bottomContainerView.rightAnchor, constant: -24).isActive = true
        refreshButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        refreshButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    func setupBottomSeparatorView() {
        view.addSubview(bottomSeparatorView)
        bottomSeparatorView.topAnchor.constraint(equalTo: bottomContainerView.bottomAnchor).isActive = true
        bottomSeparatorView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomSeparatorView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        bottomSeparatorView.heightAnchor.constraint(equalToConstant: (1.0 / UIScreen.main.scale)).isActive = true
    }
    
    func setupMapView() {
        view.addSubview(mapView)
        mapView.topAnchor.constraint(equalTo: bottomSeparatorView.bottomAnchor).isActive = true
        mapView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mapView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    func setupLogoutButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
    }
    
    func setupActivityIndicator() {
        topContainerView.addSubview(activityIndicator)
        activityIndicator.centerYAnchor.constraint(equalTo: topContainerView.centerYAnchor).isActive = true
        activityIndicator.leftAnchor.constraint(equalTo: topContainerView.leftAnchor, constant: 8).isActive = true
    }
    
    func setupSVProgressHud() {
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setBackgroundColor(UIColor.CustomColors.midnightBlue)
        SVProgressHUD.setForegroundColor(UIColor.white)
    }
    
    func moveLocationSharingLabelRightAndShowActivityIndicator() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.locationSharingLabelLeftConstraint?.isActive = false
            self.locationSharingLabelLeftConstraint = self.locationSharingLabel.leftAnchor.constraint(equalTo: self.topContainerView.leftAnchor, constant: 32)
            self.locationSharingLabelLeftConstraint?.isActive = true
            self.activityIndicator.alpha = 1
            self.activityIndicator.startAnimating()
            self.topContainerView.layoutIfNeeded()
        }, completion: nil)
    }
    
    func moveLocationSharingLabelLeftAndDismissActivityIndicator() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.locationSharingLabelLeftConstraint?.isActive = false
            self.locationSharingLabelLeftConstraint = self.locationSharingLabel.leftAnchor.constraint(equalTo: self.topContainerView.leftAnchor, constant: 16)
            self.locationSharingLabelLeftConstraint?.isActive = true
            self.activityIndicator.alpha = 0
            self.activityIndicator.stopAnimating()
            self.topContainerView.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func switchChanged(locationSwitch: UISwitch) {
        
        if locationSwitch.isOn {
            FirebaseManager.switchUserLocationSharingStatus(isSharing: true)
            moveLocationSharingLabelRightAndShowActivityIndicator()
            locationManager?.startUpdatingLocation()
        } else {
            FirebaseManager.switchUserLocationSharingStatus(isSharing: false)
            moveLocationSharingLabelLeftAndDismissActivityIndicator()
            locationManager?.stopUpdatingLocation()
        }
    }
    
    @objc func handleLogout() {
        FirebaseManager.switchUserLocationSharingStatus(isSharing: false)
        FirebaseManager.logoutUser(success: { [weak self] (isSuccessful) -> (Void) in
            guard let welf = self else {return}
            if isSuccessful {
                locationManager?.stopUpdatingLocation()
                welf.navigationController?.popViewController(animated: true)
            }
        }) { [weak self] (error) -> (Void) in
            guard self != nil else {return}
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }
    
    @objc func handleRefreshButtonPressed() {
        SVProgressHUD.show()
        mapView.removeAnnotations(mapView.annotations)
        usersArray.removeAll()
        locationsArray.removeAll()
        updateMapViewWithFetchedUsersAndLocations()
        SVProgressHUD.dismiss(withDelay: 0.5)
    }
    
    func updateMapViewWithFetchedUsersAndLocations() {  // also sets the navigationBarTitle to the currently logged user's name
        FirebaseManager.fetchUsers { [weak self] (usersArray) -> (Void) in
            guard self != nil else {return}
            FirebaseManager.fetchLocations(usersArray: usersArray, completion: { [weak self] (locationsArray) -> (Void) in
                guard let welf = self else {return}
                welf.usersArray = usersArray
                welf.locationsArray = locationsArray
                welf.locationsArray = Sorter.matchArrayIndexes(usersArray: welf.usersArray, locationsArray: welf.locationsArray)
                for i in 0..<welf.usersArray.count {
                    
                    let regionCenter = CLLocationCoordinate2D(latitude: 45.540143, longitude: 18.697616)
                    let region = MKCoordinateRegion(center: regionCenter, latitudinalMeters: 100000, longitudinalMeters: 100000)
                    welf.mapView.setRegion(region, animated: true)
                    
                    if welf.locationsArray[i].lat != 999 && welf.locationsArray[i].lon != 999 {
                        if welf.locationsArray[i].isSharing == true {
                            let lat = welf.locationsArray[i].lat
                            let lon = welf.locationsArray[i].lon
                            
                            if welf.usersArray[i].id == FirebaseManager.getUserUID() {
                                let dropPin = UserAnnotation(id: welf.usersArray[i].id)
                                dropPin.title = "YOU"
                                let dropPinCenter = CLLocationCoordinate2D(latitude: lat , longitude: lon)
                                dropPin.coordinate = dropPinCenter
                                welf.mapView.addAnnotation(dropPin)
                            } else {
                                let dropPin = UserAnnotation(id: welf.usersArray[i].id)
                                dropPin.title = welf.usersArray[i].name
                                dropPin.subtitle = welf.usersArray[i].email
                                let dropPinCenter = CLLocationCoordinate2D(latitude: lat , longitude: lon)
                                dropPin.coordinate = dropPinCenter
                                welf.mapView.addAnnotation(dropPin)
                            }
                        } else {
                            if welf.usersArray[i].id == FirebaseManager.getUserUID() {
                                welf.setupNavigationBarTitle(user: welf.usersArray[i])
                            }
                        }
                    }
                }
            })
        }
    }
    
    func setupNavigationBarTitle(user: UserModel) {
        self.navigationItem.title = user.name
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    func setupLocationManager() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestWhenInUseAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count-1]
        if location.horizontalAccuracy > 0 {
            
            let longitude = String(location.coordinate.longitude)
            let latitude = String(location.coordinate.latitude)
            let coordinates = ["lat" : latitude, "lon" : longitude]
            FirebaseManager.uploadLocation(coordinates: coordinates)
        }
    }
    
}
