//
//  PondPresenterViewController.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 25/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class PondPresenterViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var pondArray: [PondModel]!
    var indexPath: IndexPath!
    
    let cellId = "cellId"
    
    lazy var collectionView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        cv.isPagingEnabled = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.CustomColors.midnightBlue
        layout.minimumLineSpacing = 0
        cv.delegate = self
        cv.dataSource = self
        return cv
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .black
        setupCollectionView()
        collectionView.register(PondPresentationCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    override func viewDidLayoutSubviews() {
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
    }
    
    
    func setupCollectionView() {
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PondPresentationCollectionViewCell
        cell.nameLabel.text = pondArray[indexPath.item].name
        cell.countyNameLabel.text = pondArray[indexPath.item].countyName
        cell.descriptionTextView.text = pondArray[indexPath.item].description
        cell.pictureImageView.image = pondArray[indexPath.item].picture
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pondArray.count
    }
    
    
}
