//
//  ViewController.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 24/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class ModePickerViewController: UIViewController {

    var isAlertDisplayed: Bool = false
    let key = "isAlertDisplayed"
    
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let onlineButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Pogled na lokacije ribolovaca", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.setTitleColor(UIColor.CustomColors.midnightBlue, for: .normal)
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(showLoginRegisterVC), for: .touchUpInside)
        return button
    }()
    
    let offlineButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Pregled riba i ribolovišta", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.setTitleColor(UIColor.CustomColors.midnightBlue, for: .normal)
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(showOfflineModeVC), for: .touchUpInside)
        return button
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor.CustomColors.midnightBlue
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.white.cgColor
        label.text = "Odaberite željenu opciju"
        label.textAlignment = .center
        label.textColor = .white
        label.layer.cornerRadius = 10
        label.clipsToBounds = true
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = false
        view.backgroundColor = .white
        displayIntroductionAlert()
        setupViews()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupGradientBackground()
    }
    
    func setupViews() {
        setupContainerView()
        setupTitleLabel()
        setupOnlineButton()
        setupOfflineButton()
    }
    
    func setupContainerView() {
        view.addSubview(containerView)
        containerView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        containerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 30).isActive = true
        containerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -30).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
    }
    
    func setupTitleLabel() {
        containerView.addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupOnlineButton() {
        containerView.addSubview(onlineButton)
        onlineButton.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 25).isActive = true
        onlineButton.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        onlineButton.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        onlineButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupOfflineButton() {
        containerView.addSubview(offlineButton)
        offlineButton.topAnchor.constraint(equalTo: onlineButton.bottomAnchor, constant: 25).isActive = true
        offlineButton.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        offlineButton.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        offlineButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func displayIntroductionAlert() {
        
        let defaults = UserDefaults.standard
        isAlertDisplayed = defaults.bool(forKey: key)
        
        if isAlertDisplayed == false {
            let alert = UIAlertController(title: "Dobrodošli na iFish", message: "Aplikacija iFish poslužit će Vam kao vodić za ribolov na području Slavonije i Baranje. \n\nImate mogućnost birati između dvije opcije:\nPrva opcija omogućava uvid u Vašu trenutnu lokaciju kao i lokacije ribolovaca koji dijele svoj položaj.\nDruga opcija nudi informativni sadržaj vezan za ribolovišta i ribe na našem prostoru.", preferredStyle: .alert)
            alert.view.tintColor = UIColor.CustomColors.midnightBlue
            let action = UIAlertAction(title: "U redu", style: .cancel, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            defaults.set(true, forKey: key)
        }
    }
    
    @objc func showOfflineModeVC() {
        show(OfflineModeViewController(), sender: self)
    }
    
    @objc func showLoginRegisterVC() {
        show(LoginRegisterViewController(), sender: self)
    }
    
    func setupGradientBackground() {
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [UIColor.CustomColors.midnightBlue.cgColor, UIColor.black.cgColor]
        view.layer.insertSublayer(gradient, at: 0)
    }

}

