//
//  LoginRegisterViewController.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 01/06/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginRegisterViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {

    lazy var scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.isPagingEnabled = true
        sv.delegate = self
        return sv
    }()
    
    let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.numberOfPages = 2
        return pc
    }()
    
    let registerContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var registerNameTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor.CustomColors.midnightBlue
        tf.layer.borderWidth = 1
        tf.layer.borderColor = UIColor.white.cgColor
        tf.attributedPlaceholder = NSAttributedString(string: "Unesite ime", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .ultraLight)])
        tf.textAlignment = .center
        tf.textColor = .white
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        tf.delegate = self
        return tf
    }()
    
    lazy var registerEmailTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor.CustomColors.midnightBlue
        tf.layer.borderWidth = 1
        tf.layer.borderColor = UIColor.white.cgColor
        tf.attributedPlaceholder = NSAttributedString(string: "Unesite email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .ultraLight)])
        tf.textAlignment = .center
        tf.textColor = .white
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        tf.delegate = self
        return tf
    }()
    
    lazy var registerPasswordTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor.CustomColors.midnightBlue
        tf.layer.borderWidth = 1
        tf.layer.borderColor = UIColor.white.cgColor
        tf.isSecureTextEntry = true
        tf.attributedPlaceholder = NSAttributedString(string: "Unesite zaporku", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .ultraLight)])
        tf.textAlignment = .center
        tf.textColor = .white
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        tf.delegate = self
        return tf
    }()
    
    let registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Register", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.setTitleColor(UIColor.CustomColors.midnightBlue, for: .normal)
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(handleRegisterButtonPressed), for: .touchUpInside)
        return button
    }()
    
    let loginContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var loginEmailTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor.CustomColors.midnightBlue
        tf.layer.borderWidth = 1
        tf.layer.borderColor = UIColor.white.cgColor
        tf.attributedPlaceholder = NSAttributedString(string: "Unesite email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .ultraLight)])
        tf.textAlignment = .center
        tf.textColor = .white
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        tf.delegate = self
        return tf
    }()
    
    lazy var loginPasswordTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = UIColor.CustomColors.midnightBlue
        tf.layer.borderWidth = 1
        tf.isSecureTextEntry = true
        tf.layer.borderColor = UIColor.white.cgColor
        tf.attributedPlaceholder = NSAttributedString(string: "Unesite zaporku", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .ultraLight)])
        tf.textAlignment = .center
        tf.textColor = .white
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        tf.delegate = self
        return tf
    }()
    
    let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Login", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.setTitleColor(UIColor.CustomColors.midnightBlue, for: .normal)
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(handleLoginButtonPressed), for: .touchUpInside)
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSVProgressHud()
        setupViews()
        setupToolBarDoneButton()
        checkIfUserIsLoggedIn()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupGradientBackground()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x / scrollView.frame.width == 0 {
            pageControl.currentPage = 0
        }
        else if scrollView.contentOffset.x / scrollView.frame.width == 1 {
            pageControl.currentPage = 1
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.view.frame.origin.y = self.view.frame.origin.y - 100
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.view.frame.origin.y = self.view.frame.origin.y + 100
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func setupToolbarDoneButton(textField: UITextField){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(removeKeyboard))
        toolbar.setItems([flexibleSpace,doneButton], animated: true)
        textField.inputAccessoryView = toolbar
        
    }
    
    @objc func removeKeyboard(){
        view.endEditing(true)
    }
    
    func setupToolBarDoneButton() {
        setupToolbarDoneButton(textField: loginEmailTextField)
        setupToolbarDoneButton(textField: loginPasswordTextField)
        setupToolbarDoneButton(textField: registerNameTextField)
        setupToolbarDoneButton(textField: registerEmailTextField)
        setupToolbarDoneButton(textField: registerPasswordTextField)
    }
    
    func setupViews() {
        setupScrollView()
        setupRegisterContainerView()
        setupRegisterEmailTextField()
        setupRegisterNameTextField()
        setupRegisterPasswordTextField()
        setupLoginContainerView()
        setupLoginEmailTextField()
        setupLoginPasswordTextField()
        setupLoginButton()
        setupRegisterButton()
        setupPageControl()
    }
    
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        scrollView.contentSize.width = 2 * view.frame.width
    }
    
    func setupRegisterContainerView() {
        scrollView.addSubview(registerContainerView)
        registerContainerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: view.frame.width).isActive = true
        registerContainerView.centerYAnchor.constraint(equalTo: scrollView.safeAreaLayoutGuide.centerYAnchor).isActive = true
        registerContainerView.widthAnchor.constraint(equalToConstant: 250).isActive = true
        registerContainerView.heightAnchor.constraint(equalToConstant: 215).isActive = true
    }
    
    func setupRegisterEmailTextField() {
        registerContainerView.addSubview(registerEmailTextField)
        registerEmailTextField.centerXAnchor.constraint(equalTo: registerContainerView.centerXAnchor).isActive = true
        registerEmailTextField.topAnchor.constraint(equalTo: registerContainerView.topAnchor).isActive = true
        registerEmailTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        registerEmailTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupRegisterNameTextField() {
        registerContainerView.addSubview(registerNameTextField)
        registerNameTextField.centerXAnchor.constraint(equalTo: registerContainerView.centerXAnchor).isActive = true
        registerNameTextField.topAnchor.constraint(equalTo: registerEmailTextField.bottomAnchor, constant: 5).isActive = true
        registerNameTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        registerNameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupRegisterPasswordTextField() {
        registerContainerView.addSubview(registerPasswordTextField)
        registerPasswordTextField.centerXAnchor.constraint(equalTo: registerContainerView.centerXAnchor).isActive = true
        registerPasswordTextField.topAnchor.constraint(equalTo: registerNameTextField.bottomAnchor, constant: 5).isActive = true
        registerPasswordTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        registerPasswordTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupRegisterButton() {
        registerContainerView.addSubview(registerButton)
        registerButton.centerXAnchor.constraint(equalTo: registerContainerView.centerXAnchor).isActive = true
        registerButton.topAnchor.constraint(equalTo: registerPasswordTextField.bottomAnchor, constant: 5).isActive = true
        registerButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupLoginContainerView() {
        scrollView.addSubview(loginContainerView)
        loginContainerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        loginContainerView.centerYAnchor.constraint(equalTo: scrollView.safeAreaLayoutGuide.centerYAnchor).isActive = true
        loginContainerView.widthAnchor.constraint(equalToConstant: 250).isActive = true
        loginContainerView.heightAnchor.constraint(equalToConstant: 160).isActive = true
    }
    
    func setupLoginEmailTextField() {
        loginContainerView.addSubview(loginEmailTextField)
        loginEmailTextField.topAnchor.constraint(equalTo: loginContainerView.topAnchor).isActive = true
        loginEmailTextField.centerXAnchor.constraint(equalTo: loginContainerView.centerXAnchor).isActive = true
        loginEmailTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        loginEmailTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupLoginPasswordTextField() {
        loginContainerView.addSubview(loginPasswordTextField)
        loginPasswordTextField.topAnchor.constraint(equalTo: loginEmailTextField.bottomAnchor, constant: 5).isActive = true
        loginPasswordTextField.centerXAnchor.constraint(equalTo: loginContainerView.centerXAnchor).isActive = true
        loginPasswordTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        loginPasswordTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    func setupLoginButton() {
        loginContainerView.addSubview(loginButton)
        loginButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        loginButton.topAnchor.constraint(equalTo: loginPasswordTextField.bottomAnchor, constant: 5).isActive = true
        loginButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupPageControl() {
        view.addSubview(pageControl)
        pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        pageControl.widthAnchor.constraint(equalToConstant: 25).isActive = true
        pageControl.heightAnchor.constraint(equalToConstant: 10).isActive = true
        pageControl.currentPage = 0
    }
    
    @objc func handleRegisterButtonPressed() {
        guard let name = registerNameTextField.text, let email = registerEmailTextField.text, let password = registerPasswordTextField.text, registerNameTextField.text != "", registerEmailTextField.text != "", registerPasswordTextField.text != "" else {SVProgressHUD.showError(withStatus: "Unesite sve potrebne podatke."); return}
        SVProgressHUD.show()
        FirebaseManager.registerUser(name: name, email: email, password: password, success: { [weak self] (isSuccessful) -> (Void) in
            guard let welf = self else {return}
            if isSuccessful {
                SVProgressHUD.dismiss()
                welf.navigationController?.show(MapViewController(), sender: self)
            }
        }) { (err) -> (Void) in
            if let error = err {
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }
    }
    
    @objc func handleLoginButtonPressed() {
        guard let email = loginEmailTextField.text, let password = loginPasswordTextField.text, loginEmailTextField.text != "" && loginPasswordTextField.text != "" else {SVProgressHUD.showError(withStatus: "Unesite sve potrebne podatke."); return}
        SVProgressHUD.show()
        FirebaseManager.loginUser(email: email, password: password, success: { [weak self] (isSuccessful) -> (Void) in
            guard let welf = self else {return}
            if isSuccessful {
                SVProgressHUD.dismiss()
                welf.navigationController?.show(MapViewController(), sender: self)
            }
        }) { (err) -> (Void) in
            if let error = err {
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            }
        }
    }
    
    func checkIfUserIsLoggedIn() {
        if FirebaseManager.getUserUID() != nil {
            navigationController?.show(MapViewController(), sender: self)
        }
    }
    
    func setupGradientBackground() {
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [UIColor.CustomColors.midnightBlue.cgColor, UIColor.black.cgColor]
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    func setupSVProgressHud() {
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setBackgroundColor(UIColor.CustomColors.midnightBlue)
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setMaximumDismissTimeInterval(1)
    }
    
}
