//
//  FirebaseManager.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 01/06/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import Foundation
import Firebase
import CoreLocation

class FirebaseManager {
    
    let shared = FirebaseManager()
    
    static func registerUser(name: String, email: String, password: String, success: @escaping (Bool) -> (Void), failure: @escaping (Error?) -> (Void)) {
        
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if error != nil {
                failure(error)
                return
            }
            guard let uid = Auth.auth().currentUser?.uid else {return}
            let locationID = NSUUID().uuidString.replacingOccurrences(of: "-", with: "")
            let values = ["name" : name, "email" : email, "id" : uid, "location" : locationID]
            Database.database().reference().child("users").child(uid).updateChildValues(values, withCompletionBlock: { (error, reference) in
                if error != nil {
                    failure(error)
                    return
                }
                let locationValues = ["id" : locationID, "lat" : "999", "lon" : "999", "isSharing" : "false"]
                Database.database().reference().child("locations").child(locationID).updateChildValues(locationValues, withCompletionBlock: { (err, ref) in
                    if err != nil {
                        failure(err)
                        return
                    }
                     success(true)
                })
            })
        }
    }
    
    static func loginUser(email: String, password: String, success: @escaping (Bool) -> (Void), failure: @escaping (Error?) -> (Void)) {
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if error != nil {
                print("Couldn't login user")
                failure(error)
                return
            }
            //Sucessfully logged in
            success(true)
        }
    }
    
    static func getUserUID() -> String? {
        return Auth.auth().currentUser?.uid
    }
    
    static func logoutUser(success: (Bool) -> (Void), error: (Error) -> (Void)) {
        do {
            try Auth.auth().signOut()
        } catch let err{
            error(err)
            return
        }
        success(true)
    }
    
    static func uploadLocation(coordinates: [String : String]) {
        if let uid = getUserUID() {
            getCurrentUserLocationID (uid: uid) { (id) -> (Void) in
                if let locationID = id {
                    Database.database().reference().child("locations").child(locationID).updateChildValues(coordinates, withCompletionBlock: { (error, ref) in
                        if let err = error {
                            print(err)
                        }
                    })
                }
            }
            
            
        } else {
            print("User isnt logged in")
        }
    }
    
    static func getCurrentUserLocationID(uid: String, success: @escaping (String?) -> (Void)) {
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value) { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject] {
                if let locationID = dictionary["location"] as? String {
                    success(locationID)
                }
            }
        }
    }
    
    static func fetchUsers(completion: @escaping ([UserModel]) -> (Void)) {
        var usersArray = [UserModel]()
        Database.database().reference().child("users").observeSingleEvent(of: .value) { (snapshot) in
            let numberOfChilds = snapshot.childrenCount
            Database.database().reference().child("users").observe(.childAdded, with: { (snap) in
                if let dictionary = snap.value as? [String : AnyObject] {
                    if let name = dictionary["name"] as? String, let email = dictionary["email"] as? String, let id = dictionary["id"] as? String, let locationID = dictionary["location"] as? String {
                        usersArray.append(UserModel(name: name, email: email, id: id, location: locationID))
//                        print(usersArray.count)
                        if numberOfChilds == usersArray.count {
                            completion(usersArray)
                        }
                    }
                }
            })
        }
    }
    
    static func fetchLocations(usersArray: [UserModel], completion: @escaping ([LocationModel]) -> (Void)) {
        var locationsArray = [LocationModel]()
        print("numberOfItems: " + "\(usersArray.count)")
        Database.database().reference().child("locations").observe(.childAdded) { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject] {
                if let locationID = dictionary["id"] as? String, let latitude = dictionary["lat"] as? NSString, let longitude = dictionary["lon"] as? NSString, let isSharing = dictionary["isSharing"] as? String {
                    locationsArray.append(LocationModel(lat: latitude.doubleValue, lon: longitude.doubleValue, id: locationID, isSharing: isSharing))
                    if usersArray.count == locationsArray.count {
                        completion(locationsArray)
                    }
                }
            }
        }
    }
    
    static func switchUserLocationSharingStatus(isSharing: Bool) {
        guard let uid = getUserUID() else {return}
        getCurrentUserLocationID(uid: uid) { (id) -> (Void) in
            if isSharing == true {
                guard let locationID = id else {return}
                let values = ["isSharing" : "true"]
                Database.database().reference().child("locations").child(locationID).updateChildValues(values, withCompletionBlock: { (error, ref) in
                    if let err = error {
                        print(err)
                    }
                })
            } else {
                guard let locationID = id else {return}
                let values = ["isSharing" : "false"]
                Database.database().reference().child("locations").child(locationID).updateChildValues(values, withCompletionBlock: { (error, ref) in
                    if let err = error {
                        print(err)
                    }
                })
            }
        }
    }
    
}
