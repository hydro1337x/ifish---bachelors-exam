//
//  Sorter.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 04/06/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import Foundation

class Sorter {
    
    static let shared = Sorter()
    
    static func matchArrayIndexes(usersArray: [UserModel], locationsArray: [LocationModel]) -> [LocationModel] {
        var tempArray = [LocationModel]()
        for _ in 0..<usersArray.count {
            tempArray.append(LocationModel(lat: 888, lon: 888, id: "???", isSharing: "false"))
        }
        for i in 0..<usersArray.count {
            for j in 0..<usersArray.count {
                if usersArray[i].location == locationsArray[j].id {
                    tempArray.insert(locationsArray[j], at: i)
                }
            }
        }
        return tempArray
    }
    
}
