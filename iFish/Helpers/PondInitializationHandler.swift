//
//  PondInitializationHandler.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 25/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class PondInitializationHandler {
    
    let namesArray = ["Stara Drava", "Ribnjaci ŠRD Čepina", "Jezera Bizovac", "Jezero Borovik", "Jezero Jošava", "Jezero Lapovac"]
    let countyNamesArray = ["Osječko - baranjska", "Osječko - baranjska", "Osječko - baranjska", "Osječko - baranjska", "Osječko - baranjska", "Osječko - baranjska"]
    let pictureNamesArray = ["stara_drava", "cepin", "bizovac", "borovik", "josava", "lapovac"]
    let descriptionsArray =
        ["Ukupna vodena površina ovog starog dravskog toka iznosi 56 hektara. Podijeljena je mostom i ustavom na dva jezera - lijevo i desno. Lijevo jezero zaraslo je u trsku i u njemu dominira kapitalna štuka. Tu se udomaćio i divlji dunavski šaran, koji je na muljevitom, hranom bogatom dnu, pronašao idealno hranilište. Cijelom dužinom lijevog jezera mogu se loviti i golemi patuljasti somovi. Idući prema zapadu dolazi se do dijela koji se zove Vodar. Na Vodaru se odlično ribolove šaranke i babuške, ali i deverika, crvenperka, žutooka (bodorka), jez, šaran, amur i štuka.",
         "Ribnjaci zauzimaju 9 hektara voda od toga 6 hektara za športski ribolov. Skrenete li s magistralnog puta Đakovo-Osijek za Vladislavce, 800 m do ribnjaka je uređen prilaz, a putokazi ce vas odvesti do vode. Sam početak nastanka ribnjaka počinje još 1957. g. kada su iskopane jame za potrebe ciglane.Tokom godina, to su postale ekološke jame za odlaganje otpada. ŠRD ”Čepin” je drustvo koje je osnovano 1995. godine, te od tada postoji kao kompleks od 2 Bajera. U ribnjacima živi 15 vrsta riba, a najbrojnije su šaran(riječni), šaran ribnjicarski, amur, smuđ. Godišnje se u vodu poribi oko 3000 kg plemenite ribe. Na ribnjacima volonterski radi prva hrvatska ribočuvarica koja poslužuje članove sa osvježavajucim napitcima, dnevnim i godišnjim ribolovnicama.",
         "Dugogodišnjim iskapanjem gline nastalo je nekoliko umjetnih jezera, čija dubina seže do 6 metara i više, a u njima je voda izuzetno čista, gotovo pitka, te je prema riječima redovnih posjetitelja, sigurno najčišća ribolovna i kupališna voda u okolici. Procjenjuje se da je ukupna vodena površina svih bajera veća od 15 hektara», tvrdi on, čije nam riječi potvrđuju i mnogi Bizovčani, posebno ribolovci koji obitavaju često na ovom mjestu i koji kućama odavde nose zavidno bogate ulove. Tako čista voda naime, obiluje raznovrsnom ribom, smuđevima, šaranima, štukama, linjacima, kucinima, somovima, bandarima i drugim vrstama bijele ribe. Vodama gospodari ribolovno društvo “Bandar” Bizovac, koje se brine o redovitom prihranjivanju i čuvanju voda i okoliša od onečišćenja, a vrijedni i marljivi ribiči tijekom godine redovno organiziraju ribička natjecanja. Pored velikog bajera izgrađen je lijep ribički dom u kojem se mogu organizirati obiteljske svečanosti, prijateljska druženja i druga zabavna događanja. ",
         "Rijeka Vuka izvire iz sjeveroistočnih izdanaka Krstova kod sela Paučja. Na samom početku njena toka, izgradnjom brane 1978. godine, stvoreno je umjetno jezero Borovik. Jezero „Borovik“ nalazi se zapadno od Đakova i dvokrakog je oblika.  Jezero im a površinu od 160 ha s dubinom od 15 m, dužine cca 7.000 m, prosječne širine cca 300 m i ukupne zapremine 8 500 000 m3 vode. Borovik je jedno od najatraktivnijih mjesta za šaranski ribolov ( kapitalni šarani težine do 30 kg) i ribolov pastrvskog grgeča(težine 2 i više kg) u Hrvatskoj i ovom dijelu Europe. Osim spomenutih vrsta riba  u Boroviku obitava amur, som, smuđ, štuka, grgeč, linjak, patuljasti somić, babuška, crvenperka, žutooka i uklija.",
         "Jezero “Jošava“ nalazi se oko 2 km sjevero-sjeveroistočno od grada Đakova. Proteže se od željezničkog nasipa Đakovo-Osijek do umjetno podignute zemljane brane koja se nalazi oko 2 km nizvodno. To je zapravo jedno umjetno jezero izgrađeno 1963/64. godine. Prvenstveno su ga namijenili za uzgoj riba, šport i rekreaciju. Napravljeno je tako da je pregrađeno korito potoka Jošava zemljanom branom betonskom ustavom. S time je postignuto da se voda akumulira ispred brane, potapa okolna polja i stvara jezero. Zemljana brana jezera popravljena je 1993/94. godine. U temeljni ispust je ugrađen novi zasun, kojim se regulira razina vode jezera odnosno ispuštanje vode iz jezera. Oblik jezera je duguljast, proteže se od zapada prema istoku. Dugačko je oko 4,5 km, a širina iznosi 100 do 180 m. Obalu jezera sačinjavaju željeznički nasip Đakovo-Osijek na zapadu, zemljani nasip sa temeljnim ispustom na istoku i lesna uzvišenja sa južne i sjeverne strane. Dubina jezera kreće se od 70 cm do 3m. Na zapadu je nešto pliće, a na istoku nešto dublje. Ipak najveća je dubina u sredini gdje se proteže jedan kanal, bivše korito potoka Jošava, u smjeru zapad-istok. Od kanala bočno prema obali sve je pliće. Oko 80 % površine jezera je čisto, a oko 20 % je uglavnom na rubovima, obraslo trskom, rogozom i drugim vodenim biljem. U vodi se nalaze još u dovoljnoj količini podvodne biljke, vrste iz porodice mrijesak. Oko jezera nalaze se određene poljoprivredne površine i nešto sađenih šuma kod Kolokušice.Vrste riba koje su navedene za jezero Borovik nalaze se i u Jošavi.",
         "Lapovac je umjetno jezero koje se nalazi 3 km od središta grada Našica. Glavno je i najposjećenije izletište Našičana. Nastalo je 1993. godine, izgradnjom brane na potoku Lapovac i tako je spriječeno poplavljivanje naselja nizvodno. Ukupna površina je oko 80 hektara, a prosječna dubina je 4 metara. Jezero i jezerska okolica je uređena, pa ga posjetitelji koriste za odlaske na piknik, ribolov i kupanje. Pored izletničkog, Lapovac je ostvario i športsku namjenu, pa se na njemu održavaju i športska natjecanja, među ostalim bilo je mjestom održavanja europskog prvenstva u kajaku. Jezero je bogato ribom, kojom gospodari ŠUD Šaran Našice, a također se održavaju natjecanja u sportskom ribolovu."
    ]
    
    static let shared = PondInitializationHandler()
    
    func fillArray() -> [PondModel] {
        var pondsArray: [PondModel] = []
        
        for i in 0..<namesArray.count {
            let name = namesArray[i]
            let countyName = countyNamesArray[i]
            let pictureName = pictureNamesArray[i]
            let description = descriptionsArray[i]
            let pond = createPondFromParameters(name: name, countyName: countyName, pictureName: pictureName, description: description)
            pondsArray.append(pond)
        }
        
        return pondsArray
        
    }
    
    func createPondFromParameters(name: String, countyName: String, pictureName: String, description: String) -> PondModel {
        return PondModel(name: name, countyName: countyName, picture: UIImage(named: pictureName) ?? UIImage(), description: description)
    }
    
}
