//
//  PondContainerCell.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 25/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class PondsContainerCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    weak var delegate: PondsContainerCellDelegate?
    
    let cellId = "cellId"
    var pondsArray: [PondModel]!
    
    lazy var collectionView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        layout.minimumLineSpacing = 2
        cv.delegate = self
        cv.dataSource = self
        return cv
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        pondsArray = PondInitializationHandler.shared.fillArray()
        setupCollectionView()
        collectionView.register(PondCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupGradientBackground()
    }
    
    func setupCollectionView() {
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pondsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PondCell
        cell.nameLabel.text = pondsArray[indexPath.item].name
        cell.countyNameLabel.text = pondsArray[indexPath.item].countyName
        cell.pictureImageView.image = pondsArray[indexPath.item].picture
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: (frame.height - 8) / 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.transferPondArrayAndSelectedIndexPath(pondArray: pondsArray, indexPath: indexPath)
    }
    
    func setupGradientBackground() {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [UIColor.CustomColors.midnightBlue.cgColor, UIColor.black.cgColor]
        layer.insertSublayer(gradient, at: 0)
    }
    
}
