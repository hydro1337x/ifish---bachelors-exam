//
//  FishPresentationCellCollectionViewCell.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 01/06/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class FishPresentationCollectionViewCell: UICollectionViewCell, UITextViewDelegate {
    
    var upperContainerHeight: CGFloat!
    
    let upperContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let bottomContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.numberOfLines = 2
        label.backgroundColor = .clear
        label.text = "Šaran"
        return label
    }()
    
    let latinNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18)
        label.numberOfLines = 2
        label.backgroundColor = .clear
        label.text = "Per aspera ad astra fwafawf faf f"
        return label
    }()
    
    let pictureImageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .green
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 5
        return iv
    }()
    
    lazy var descriptionTextView: UITextView = {
        let tf = UITextView()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.textColor = .white
        tf.backgroundColor = .clear
        tf.contentMode = UIView.ContentMode.scaleToFill
        tf.font = UIFont.systemFont(ofSize: 18)
        tf.isEditable = false
        tf.delegate = self
        return tf
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        upperContainerHeight = (frame.height - safeAreaInsets.top - safeAreaInsets.bottom) / 5
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupGradientBackground()
    }
    
    func setupViews() {
        setupUpperContainer()
        setupPictureImageView()
        setupNameLabel()
        setupLatinNameLabel()
        setupBottomContainerView()
        setupDescriptionTextView()
    }
    
    func setupUpperContainer() {
        addSubview(upperContainerView)
        upperContainerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        upperContainerView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        upperContainerView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        upperContainerView.heightAnchor.constraint(equalToConstant: upperContainerHeight).isActive = true
    }
    
    func setupPictureImageView() {
        upperContainerView.addSubview(pictureImageView)
        pictureImageView.topAnchor.constraint(equalTo: upperContainerView.topAnchor).isActive = true
        pictureImageView.leftAnchor.constraint(equalTo: upperContainerView.leftAnchor, constant: 8).isActive = true
        pictureImageView.bottomAnchor.constraint(equalTo: upperContainerView.bottomAnchor).isActive = true
        pictureImageView.widthAnchor.constraint(equalToConstant: frame.width / 2).isActive = true
    }
    
    func setupNameLabel() {
        upperContainerView.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: upperContainerView.topAnchor, constant: 8).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: pictureImageView.rightAnchor, constant: 8).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: upperContainerView.rightAnchor, constant: -8).isActive = true
        print(upperContainerView.frame.height)
        nameLabel.heightAnchor.constraint(equalToConstant: (upperContainerHeight - 24) / 2).isActive = true
    }
    
    func setupLatinNameLabel() {
        upperContainerView.addSubview(latinNameLabel)
        latinNameLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8).isActive = true
        latinNameLabel.leftAnchor.constraint(equalTo: pictureImageView.rightAnchor, constant: 8).isActive = true
        latinNameLabel.bottomAnchor.constraint(equalTo: upperContainerView.bottomAnchor, constant: -8).isActive = true
        latinNameLabel.rightAnchor.constraint(equalTo: upperContainerView.rightAnchor, constant: -8).isActive = true
    }
    
    func setupBottomContainerView() {
        addSubview(bottomContainerView)
        bottomContainerView.topAnchor.constraint(equalTo: upperContainerView.bottomAnchor).isActive = true
        bottomContainerView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomContainerView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomContainerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    func setupDescriptionTextView() {
        bottomContainerView.addSubview(descriptionTextView)
        descriptionTextView.topAnchor.constraint(equalTo: bottomContainerView.topAnchor, constant: 8).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: bottomContainerView.leftAnchor, constant: 8).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo: bottomContainerView.rightAnchor, constant: -8).isActive = true
        descriptionTextView.bottomAnchor.constraint(equalTo: bottomContainerView.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
    }
    
    func setupGradientBackground() {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [UIColor.CustomColors.midnightBlue.cgColor, UIColor.black.cgColor]
        layer.insertSublayer(gradient, at: 0)
    }
    
}
