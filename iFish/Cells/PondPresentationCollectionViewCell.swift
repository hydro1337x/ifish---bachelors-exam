//
//  PondPresentationCollectionViewCell.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 01/06/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class PondPresentationCollectionViewCell: UICollectionViewCell, UITextViewDelegate {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.numberOfLines = 2
        label.backgroundColor = UIColor.CustomColors.midnightBlue
        label.text = "Šaran"
        return label
    }()
    
    let countyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18)
        label.numberOfLines = 2
        label.backgroundColor = UIColor.CustomColors.midnightBlue
        label.text = "Per aspera ad astra fwafawf faf f"
        return label
    }()
    
    let pictureImageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .clear
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 5
        return iv
    }()
    
    lazy var descriptionTextView: UITextView = {
        let tf = UITextView()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.textColor = .white
        tf.backgroundColor = .clear
        tf.contentMode = UIView.ContentMode.scaleToFill
        tf.font = UIFont.systemFont(ofSize: 18)
        tf.isEditable = false
        tf.delegate = self
        return tf
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupGradientBackground()
        nameLabel.roundCorners(corners: [.topLeft, .topRight], radius: 5)
        countyNameLabel.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 5)
    }
    
    func setupViews() {
        setupPictureImageView()
        setupNameLabel()
        setupCountyNameLabel()
        setupDescriptionTextView()
    }
    
    func setupPictureImageView() {
        addSubview(pictureImageView)
        pictureImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        pictureImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        pictureImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        pictureImageView.heightAnchor.constraint(equalToConstant: (frame.height - safeAreaInsets.top - safeAreaInsets.bottom) / 3).isActive = true
    }
    
    func setupNameLabel() {
        addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: pictureImageView.bottomAnchor, constant: 8).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    func setupCountyNameLabel() {
        addSubview(countyNameLabel)
        countyNameLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        countyNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        countyNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        countyNameLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
    }
    
    func setupDescriptionTextView() {
        addSubview(descriptionTextView)
        descriptionTextView.topAnchor.constraint(equalTo: countyNameLabel.bottomAnchor, constant: 8).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        descriptionTextView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    func setupGradientBackground() {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [UIColor.CustomColors.midnightBlue.cgColor, UIColor.black.cgColor]
        layer.insertSublayer(gradient, at: 0)
    }
    
}
