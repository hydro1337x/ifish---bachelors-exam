//
//  FishCell.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 25/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class FishCell: UICollectionViewCell {
    
    var delegate: FishContainerCellDelegate?
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.numberOfLines = 2
        label.text = "Šaran"
        return label
    }()
    
    let latinNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18)
        label.numberOfLines = 2
        label.text = "Per aspera ad astra fwafawf faf f"
        return label
    }()
    
    let pictureImageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = UIView.ContentMode.scaleToFill
        iv.backgroundColor = .green
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 5
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        setupPictureImageView()
        setupNameLabel()
        setupLatinNameLabel()
    }
    
    func setupPictureImageView() {
        addSubview(pictureImageView)
        pictureImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        pictureImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        pictureImageView.widthAnchor.constraint(equalToConstant: (frame.width - 24) / 2).isActive = true
        pictureImageView.heightAnchor.constraint(equalToConstant: frame.height - 16).isActive = true
    }
    
    func setupNameLabel() {
        addSubview(nameLabel)
        nameLabel.leftAnchor.constraint(equalTo: pictureImageView.rightAnchor, constant: 8).isActive = true
        nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: (frame.height - 40) / 2).isActive = true
    }
    
    func setupLatinNameLabel() {
        addSubview(latinNameLabel)
        latinNameLabel.leftAnchor.constraint(equalTo: pictureImageView.rightAnchor, constant: 8).isActive = true
        latinNameLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8).isActive = true
        latinNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        latinNameLabel.heightAnchor.constraint(equalToConstant: (frame.height - 40) / 2).isActive = true
    }
    
    
}
