//
//  LocationModel.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 01/06/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import Foundation
import CoreLocation

class LocationModel {
    
    var lat: CLLocationDegrees
    var lon: CLLocationDegrees
    var id: String
    var isSharing: Bool
    
    init(lat: CLLocationDegrees, lon: CLLocationDegrees, id: String, isSharing: String) {
        if isSharing == "true" {
            self.isSharing = true
        } else {
            self.isSharing = false
        }
        self.lat = lat
        self.lon = lon
        self.id = id
    }
}
