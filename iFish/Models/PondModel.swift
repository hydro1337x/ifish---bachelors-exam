//
//  PondModel.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 25/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class PondModel {
    
    var name: String
    var countyName: String
    var picture: UIImage
    var description: String
    
    init(name: String, countyName: String, picture: UIImage, description: String) {
        self.name = name
        self.countyName = countyName
        self.picture = picture
        self.description = description
    }
    
}
