//
//  FishModel.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 25/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class FishModel {
    
    var name: String
    var latinName: String
    var picture: UIImage
    var description: String
    
    init(name: String, latinName: String, picture: UIImage, description: String) {
        self.name = name
        self.latinName = latinName
        self.picture = picture
        self.description = description
    }
    
}
