//
//  UserModel.swift
//  iFish
//
//  Created by Benjamin Mecanovic on 01/06/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import Foundation

class UserModel {
    
    var name: String
    var email: String
    var id: String
    var location: String
    
    init(name: String, email: String, id: String, location: String) {
        self.name = name
        self.email = email
        self.id = id
        self.location = location
    }
    
    
}
